﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using c = ParseAndSort.Globals.Constants;


namespace ParseAndSort.Globals.POCO
{
	public class Record
	{
		public string LastName { get; set; }
		public string FirstName { get; set; }
		public string Gender { get; set; }
		public string FavoriteColor { get; set; }
		public DateTime DOB { get; set; }

		private string padString;
		private string recordFormat    = "{0} : {1} : {2} : {3} : {4}";
		private string delimitedFormat = "{0}{5}{1}{5}{2}{5}{3}{5}{4}";

		public Record()
		{
			padString = CreatePadString(c.Constants.TabLength);
		}

		public Record(string record) : this()
		{
			char delimiter = GetDelimiter(record);
			Parse(record, delimiter);
		}

		public Record(string record, char delimiter) : this()
		{
			Parse(record, delimiter);
		}

		public string ToDelimited(string delimiter = ", ")
		{
			string output = string.Format(delimitedFormat,	LastName,
															FirstName,
															Gender,
															FavoriteColor,
															DOB.ToString("MM/dd/yyyy"),
															delimiter);
			return output;
		}

		public string ToRecord()
		{
			string output = string.Format(recordFormat, Tab(LastName), 
														Tab(FirstName), 
														Tab(Gender), 
														Tab(FavoriteColor),
														Tab(DOB.ToString("MM/dd/yyyy")));
			return output;
		}

		public string CreateHeader()
		{
			string output = string.Format(recordFormat, Tab("Last Name"),
											            Tab("First Name"),
											            Tab("Gender"),
											            Tab("Favorite Color"),
											            Tab("DOB"));

			return output;											   
		}

		public string CreateUnderline()
		{
			string output = string.Format(recordFormat, Tab("---------"),
														Tab("----------"),
														Tab("------"),
														Tab("--------------"),
														Tab("---"));

			return output;
		}

		private char GetDelimiter(string record)
		{
			if (record.Contains("|"))
				return '|';
			else if (record.Contains(","))
				return ',';
			else 
				return ' ';
		}

		private string Tab(string field)
		{
			string tab = field + padString;
			tab = tab.Substring(0, c.Constants.TabLength);

			return tab;
		}

		private void Parse(string record, char delimiter)
		{
			string[] r = record.Split(delimiter);
			int i = 0;

			LastName      = r[i++].Trim();
			FirstName     = r[i++].Trim();
			Gender        = r[i++].Trim();
			FavoriteColor = r[i++].Trim();
			DOB           = DateTime.Parse(r[i++].Trim());
		}

		private string CreatePadString(int tabLength)
		{
			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < tabLength; i++)
			{
				sb.Append(" ");
			}

			return sb.ToString();
		}
	}
}
