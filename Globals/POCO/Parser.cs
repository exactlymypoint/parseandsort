﻿using ParseAndSort.Globals.Interfaces;
using ParseAndSort.Globals.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParseAndSort.Globals.POCO
{
	public class Parser : IParser
	{
		public IEnumerable<Record> ParseLines(string[] fileLines)
		{
			Record record;
			List<Record> records = new List<Record>();

			foreach (string line in fileLines)
			{
				if (!string.IsNullOrEmpty(line.Trim()))
				{
					record = new Record(line);
					records.Add(record);
				}
			}

			return records;
		}
	}
}