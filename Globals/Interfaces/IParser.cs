﻿using ParseAndSort.Globals.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParseAndSort.Globals.Interfaces
{
	public interface IParser
	{
		IEnumerable<Record> ParseLines(string[] fileLines);
	}
}
