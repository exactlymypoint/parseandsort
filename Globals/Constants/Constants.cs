﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParseAndSort.Globals.Constants
{
	public class Constants
	{
		public const int MaxFiles  = 3;
		public const int TabLength = 15;
	}
}
