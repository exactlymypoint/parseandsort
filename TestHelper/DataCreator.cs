﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using ParseAndSort.Globals.POCO;
using c = ParseAndSort.Globals.Constants;

namespace TestHelper
{
	public class DataCreator
	{
		Fixture fixture = new Fixture();

		public string[] CreateNamesLines()
		{
			string[] lines =
			{
				"LastName | FirstName | Gender | FavoriteColor | 2/26/1955",
				"",
				"LastName,  FirstName,  Gender,  FavoriteColor,  2/26/1955",
				"         ",
				"LastName FirstName Gender FavoriteColor 2/26/1955"
			};

			return lines;
		}

		public Record CreateRecord()
		{
			Record r = new Record
			{
				LastName      = "LastName",
				FirstName     = "FirstName",
				Gender        = "Gender",
				FavoriteColor = "FavoriteColor",
				DOB           = new DateTime(1955, 2, 26)
			};

			return r;
		}

		public string CreatePadString(string pad)
		{
			string padded = pad + "                                              ";

			if (padded.Length < c.Constants.TabLength)
				throw new Exception("Padded length not greater than tab length constant.");

			padded = padded.Substring(0, c.Constants.TabLength);

			return padded;
		}

		public string[] CreateSortDataStringArray()
		{
			Fixture fixture = new Fixture();

			List<string> records = new List<string>();
			Record record;
			string oneLine;

			for (int i = 0; i < 5; i++)
			{
				record  = fixture.Build<Record>().Create();
				oneLine = record.ToDelimited();
				records.Add(oneLine);
			}

			return records.ToArray();
		}

		public List<Record> CreateSortDataList()
		{
			Fixture fixture = new Fixture();

			List<Record> records = new List<Record>();
			Record record;

			for (int i = 0; i < 5; i++)
			{
				record = fixture.Build<Record>().Create();
				records.Add(record);
			}

			return records;
		}

		public string[] CreateFileLines()
		{
			List<string> buildLines = new List<string>();
			string lineFormat = "LN{0} FN{0} G{0} FC{0} {0}/26/1955";

			buildLines.Add("");
			buildLines.Add("       ");

			for (int i = 1; i < 4; i++)
			{
				buildLines.Add(string.Format(lineFormat, i));
			}

			return buildLines.ToArray();
		}

		public List<Record> CreateRecordList()
		{
			List<Record> records = new List<Record>();
			string lineFormat = "LN{0} FN{0} G{0} FC{0} {0}/26/1955";
			Record record;

			for (int i = 1; i < 4; i++)
			{
				record = new Record(string.Format(lineFormat, i));
				records.Add(record);
			}

			return records;
		}
	}
}
