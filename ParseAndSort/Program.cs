﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;


namespace Console.ParseAndSort
{
	class Program
	{
		private static IContainer Container;

		static void Main(string[] args)
		{
			Container = ContainerConfig.ConfigureConsoleApp();

			using (var scope = Container.BeginLifetimeScope())
			{
				ParseAndSortMain pas = scope.Resolve<ParseAndSortMain>();
				pas.Run();
			}
		}
	}
}
