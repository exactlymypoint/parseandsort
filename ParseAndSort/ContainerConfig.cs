﻿using Autofac;
using ParseAndSort.Globals.Interfaces;
using ParseAndSort.Globals.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console.ParseAndSort
{
	public static class ContainerConfig
	{
		public static IContainer ConfigureConsoleApp()
		{
			var builder = new ContainerBuilder();

			builder.RegisterType<ParseAndSortMain>();
			builder.RegisterType<ConsoleIO>().As<IInputOutput>();
			builder.RegisterType<Parser>().As<IParser>();

			return builder.Build();
		}
	}



}
