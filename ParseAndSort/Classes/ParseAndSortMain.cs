﻿using ParseAndSort.Globals.Interfaces;
using ParseAndSort.Globals.POCO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using ParseAndSort.Globals.Constants;

namespace Console.ParseAndSort
{
	public class ParseAndSortMain : IApplication
	{
		private IInputOutput inputOutput;
		private IParser		 parser;

		private List<Record> records;

		public ParseAndSortMain(IInputOutput inputOutput, IParser parser)
		{
			this.inputOutput = inputOutput;
			this.parser      = parser;
		}

		public void Run()
		{
			string[] filenames;

			filenames = GetFileNames();

			foreach (string f in filenames)
			{
				ReadFile(f);
			}

			inputOutput.DisplaySort(records);
			inputOutput.EndMessage();
		}

		private string[] GetFileNames()
		{
			bool badFilename   = false;
			string filename    = "";
			string[] filenames = new string[Constants.MaxFiles];

			for (int i = 0; i < Constants.MaxFiles; i++)
			{
				badFilename = false;
				do
				{
					filename = GetFileName(badFilename, i + 1);
					if (fileExists(ref filename))
						badFilename = false;
					else
						badFilename = true;

				} while (badFilename);

				filenames[i] = filename;
			}

			return filenames;
		}

		private bool fileExists(ref string filename)
		{
			bool doesFileExist = false;

			// Get the other searchable directories.
			string curPath        = Directory.GetCurrentDirectory();
			string appStartupPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string dataPath       = Directory.GetParent(appStartupPath + @"\..\..").ToString() + @"\Data\";

			doesFileExist = File.Exists(filename);

			if (!doesFileExist)
			{
				doesFileExist = File.Exists(curPath + @"\" + filename);
				if (doesFileExist)
					filename = curPath + @"\" + filename;
			}

			if (!doesFileExist)
			{
				doesFileExist = File.Exists(dataPath + filename);
				if (doesFileExist)
					filename = dataPath + filename;
			}

			return doesFileExist;
		}

		private string GetFileName(bool doesFileExist, int i)
		{
			//string hardPath = @"D:\_Sync\source\repos\ParseAndSort\ParseAndSort\Data\";

			string filename = inputOutput.GetFilename(doesFileExist, i);
			return filename;
		}

		private void ReadFile(string filename)
		{
			string[] fileLines;

			fileLines = File.ReadAllLines(filename);
			records = parser.ParseLines(fileLines).ToList();
		}
	}
}
