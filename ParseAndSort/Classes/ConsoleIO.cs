﻿using ParseAndSort.Globals.Interfaces;
using ParseAndSort.Globals.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysCon = System.Console;

namespace Console.ParseAndSort
{
	public class ConsoleIO : IInputOutput
	{
		public void DisplaySort(List<Record> records)
		{
			List<Record> sortedRecords;

			sortedRecords = records.OrderBy(o => o.Gender)
				                   .ThenBy(tb => tb.LastName)
				                   .ToList();

			WritePrompt("By gender, Lastname");
			WriteRecordsToConsole(sortedRecords);

			sortedRecords = records.OrderBy(o => o.DOB).ToList();
			WritePrompt("By birthday");
			WriteRecordsToConsole(sortedRecords);

			sortedRecords = records.OrderBy(o => o.LastName).ToList();
			WritePrompt("By last name");
			WriteRecordsToConsole(sortedRecords);
		}

		public void EndMessage()
		{
			SysCon.WriteLine();
			SysCon.WriteLine("Press ENTER key to end");

			SysCon.ReadLine();
		}

		public string GetFilename(bool badFilename, int fileNbr)
		{
			if (badFilename)
			{
				SysCon.WriteLine();
				SysCon.WriteLine("File does not exist. Please try again.");
			}

			string promptFormat = "Filename {0}: ";
			string prompt       = string.Format(promptFormat, fileNbr);

			SysCon.Write(prompt);
			string filename = SysCon.ReadLine();
			return filename;
		}

		private void WriteRecordsToConsole(List<Record> sortedRecords)
		{
			Record header = new Record();

			SysCon.WriteLine(header.CreateHeader());
			SysCon.WriteLine(header.CreateUnderline());

			foreach (Record record in sortedRecords)
			{
				SysCon.WriteLine(record.ToRecord());
			}
		}

		private void WritePrompt(string heading)
		{
			SysCon.WriteLine();
			SysCon.WriteLine(heading);

		}
	}
}