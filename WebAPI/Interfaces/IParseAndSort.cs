﻿using ParseAndSort.Globals.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace WebAPI.Interfaces
{
	public interface IParseAndSort
	{
		string ReadRecord(string dataLine);
		List<Record> SortByGender(List<Record> records);
		List<Record> SortByName(List<Record> records);
		List<Record> SortByBirthdate(List<Record> records);
	}
}