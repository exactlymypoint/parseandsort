﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ParseAndSort.Globals.POCO;
using WebAPI.Interfaces;

namespace WebAPI.Domain
{
	public class ParseAndSort : IParseAndSort
	{
		public string ReadRecord(string dataLine)
		{
			Record record = new Record(dataLine);

			return record.ToRecord();
		}

		public List<Record> SortByBirthdate(List<Record> records)
		{
			List<Record> sorted = records.OrderBy(o => o.DOB).ToList();

			return sorted;
		}

		public List<Record> SortByGender(List<Record> records)
		{
			List<Record> sorted = records.OrderBy(o => o.Gender).ToList();

			return sorted;
		}

		public List<Record> SortByName(List<Record> records)
		{
			List<Record> sorted = records.OrderBy(o => o.LastName).ToList();

			return sorted;
		}
	}
}