﻿using Newtonsoft.Json;
using ParseAndSort.Globals.Interfaces;
using ParseAndSort.Globals.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Interfaces;

namespace WebAPI.Controllers
{
    public class ParseAndSortController : ApiController
    {
		private readonly IParser       parser;
		private readonly IParseAndSort parseAndSort;

		public ParseAndSortController(IParseAndSort parseAndSort, IParser parser)
		{
			this.parseAndSort = parseAndSort;
			this.parser       = parser;
		}

		[HttpPost]
		[Route("records")]
		public IHttpActionResult ReadRecord([FromBody]string dataLine)
		{
			var retVal = parseAndSort.ReadRecord(dataLine);

			return Ok(retVal);
		}

		[HttpGet]
		[Route("records/gender")]
		public IHttpActionResult SortByGender([FromBody]List<Record> records)
		{
			var retVal = parseAndSort.SortByGender(records);

			string json = JsonConvert.SerializeObject(retVal);

			return Ok(json);
		}

		[HttpGet]
		[Route("records/name")]
		public IHttpActionResult SortByName([FromBody]List<Record> records)
		{
			var retVal = parseAndSort.SortByName(records);

			string json = JsonConvert.SerializeObject(retVal);

			return Ok(json);
		}

		[HttpGet]
		[Route("records/birthdate")]
		public IHttpActionResult SortByBirthdate([FromBody]List<Record> records)
		{
			var retVal = parseAndSort.SortByBirthdate(records);

			string json = JsonConvert.SerializeObject(retVal);

			return Ok(json);
		}


	}
}
