﻿using Autofac;
using Autofac.Integration.Mvc;
using ParseAndSort.Globals.Interfaces;
using ParseAndSort.Globals.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAPI.Domain;
using WebAPI.Interfaces;

namespace WebAPI.App_Start
{
	public class AutofacConfig
	{
		public static void RegisterDependencies()
		{
			var builder = new ContainerBuilder();

			// Register your MVC controllers. (MvcApplication is the name of
			// the class in Global.asax.)
			builder.RegisterControllers(typeof(MvcApplication).Assembly);

			// OPTIONAL: Register model binders that require DI.
			builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
			builder.RegisterModelBinderProvider();

			// You can register controllers all at once using assembly scanning...
			builder.RegisterControllers(typeof(MvcApplication).Assembly);

			builder.RegisterType<Parser>().As<IParser>();
			builder.RegisterType<Domain.ParseAndSort>().As<IParseAndSort>();

			// Set the dependency resolver to be Autofac.
			var container = builder.Build();
			DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
		}
	}
}