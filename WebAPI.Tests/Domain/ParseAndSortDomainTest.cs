﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestHelper;
using WebAPI.Domain;
using Xunit;
using PAS = ParseAndSort.Globals.POCO;

namespace WebAPI.Tests.Domain
{
	public class ParseAndSortDomainTest
	{
		private DataCreator dc;
		private string[] testLines;
		private List<PAS.Record> records;
		private WebAPI.Domain.ParseAndSort parseAndSort;

		public ParseAndSortDomainTest()
		{
			dc = new DataCreator();
			parseAndSort = new WebAPI.Domain.ParseAndSort();
		}

		[Theory]
		[InlineData("LastName | FirstName | Gender | FavoriteColor | 2/26/1955")]
		[InlineData("LastName,  FirstName,  Gender,  FavoriteColor,  2/26/1955")]
		[InlineData("LastName FirstName Gender FavoriteColor 2/26/1955")]
		public void ParseAndSortDomain_ReadLineShouldSucceed(string record)
		{
			string actual   = parseAndSort.ReadRecord(record);
			string expected = "LastName        : FirstName       : Gender          : FavoriteColor   : 02/26/1955     ";

			Assert.NotNull(actual);
			Assert.Equal(expected, actual);
			Assert.Equal(expected.Length, actual.Length);
		}

		[Fact]
		public void ParseAndSortDomain_SortByNameShouldSucceed()
		{
			records = dc.CreateSortDataList();
			List<PAS.Record> sortedRecords = parseAndSort.SortByName(records);
			PAS.Record lastRecord = sortedRecords[0];

			foreach (PAS.Record r in sortedRecords)
			{
				string current  = r.LastName;
				string previous = lastRecord.LastName;

				Assert.True(string.Compare(current, previous) >= 0);
				lastRecord = r;
			}
		}

		[Fact]
		public void ParseAndSortDomain_SortByGenderShouldSucceed()
		{
			records = dc.CreateSortDataList();
			List<PAS.Record> sortedRecords = parseAndSort.SortByGender(records);
			PAS.Record lastRecord = sortedRecords[0];

			foreach (PAS.Record r in sortedRecords)
			{
				string current  = r.Gender;
				string previous = lastRecord.Gender;

				Assert.True(string.Compare(current, previous) >= 0);
				lastRecord = r;
			}
		}

		[Fact]
		public void ParseAndSortDomain_SortByBirthdateShouldSucceed()
		{
			records = dc.CreateSortDataList();
			List<PAS.Record> sortedRecords = parseAndSort.SortByBirthdate(records);
			PAS.Record lastRecord = sortedRecords[0];

			foreach (PAS.Record r in sortedRecords)
			{
				DateTime current = r.DOB;
				DateTime previous = lastRecord.DOB;

				Assert.True(current >= previous);
				lastRecord = r;
			}
		}
	}
}
