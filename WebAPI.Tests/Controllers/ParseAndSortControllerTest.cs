﻿using ParseAndSort.Globals.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Controllers;
using TestHelper;
using Xunit;
using PAS = ParseAndSort.Globals.POCO;
using Newtonsoft.Json;
using WebAPI.Interfaces;
using Moq;
using System.Web.Http;
using System.Web.Http.Results;


namespace WebAPI.Tests.Controllers
{
	public class ParseAndSortControllerTest
	{
		private ParseAndSortController pasc;
		private DataCreator dc;
		private List<PAS.Record> testRecords;

		Mock<IParser> parser;
		Mock<IParseAndSort> parseAndSort;

		public ParseAndSortControllerTest()
		{
			parser = new Mock<IParser>();
			parseAndSort = new Mock<IParseAndSort>();

			pasc = new ParseAndSortController(parseAndSort.Object, parser.Object);
			dc = new DataCreator();
		}

		[Fact]
		public void ParseAndSortController_ReadRecordShouldSucceed()
		{
			string inParameter = "xyz";
			string expected = "abc";
			parseAndSort.Setup<string>(p => p.ReadRecord(inParameter)).Returns(expected);

			IHttpActionResult readRecord = pasc.ReadRecord(inParameter);
			var actualResults = readRecord as OkNegotiatedContentResult<string>;

			Assert.Equal(expected, actualResults.Content);
		}

		[Fact]
		public void ParseAndSortController_SortByGenderShouldSucceed()
		{
			testRecords = dc.CreateRecordList();
			string expectedJson = JsonConvert.SerializeObject(testRecords);
			parseAndSort.Setup<List<PAS.Record>>(p => p.SortByGender(testRecords)).Returns(testRecords);

			IHttpActionResult sortRecord = pasc.SortByGender(testRecords);
			var actualResults = sortRecord as OkNegotiatedContentResult<string>;

			Assert.Equal(expectedJson, actualResults.Content);
		}

		[Fact]
		public void ParseAndSortController_SortByNameShouldSucceed()
		{
			testRecords = dc.CreateRecordList();
			string expectedJson = JsonConvert.SerializeObject(testRecords);
			parseAndSort.Setup<List<PAS.Record>>(p => p.SortByName(testRecords)).Returns(testRecords);

			IHttpActionResult sortRecord = pasc.SortByName(testRecords);
			var actualResults = sortRecord as OkNegotiatedContentResult<string>;

			Assert.Equal(expectedJson, actualResults.Content);
		}

		[Fact]
		public void ParseAndSortController_BirthdateShouldSucceed()
		{
			testRecords = dc.CreateRecordList();
			string expectedJson = JsonConvert.SerializeObject(testRecords);
			parseAndSort.Setup<List<PAS.Record>>(p => p.SortByBirthdate(testRecords)).Returns(testRecords);

			IHttpActionResult sortRecord = pasc.SortByBirthdate(testRecords);
			var actualResults = sortRecord as OkNegotiatedContentResult<string>;

			Assert.Equal(expectedJson, actualResults.Content);
		}
	}
}
