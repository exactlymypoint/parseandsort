﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TestHelper;
using Xunit;
using Assert = Xunit.Assert;
using PAS = ParseAndSort.Globals.POCO;
using c = ParseAndSort.Globals.Constants;

namespace ParseAndSortGlobals.Test.POCO
{
	public class RecordTest
	{
		private PrivateObject pvtObj;
		private PAS.Record	  rec;

		public RecordTest()
		{
			rec    = new PAS.Record();
			pvtObj = new PrivateObject(rec);
		}


		[Fact]
		public void Record_0ParameterConstructorShouldSucceed()
		{
			PAS.Record r = new PAS.Record();

			Assert.NotNull(r);
		}

		[Theory]
		[InlineData("LastName | FirstName | Gender | FavoriteColor | 2/26/1955")]
		[InlineData("LastName,  FirstName,  Gender,  FavoriteColor,  2/26/1955")]
		[InlineData("LastName FirstName Gender FavoriteColor 2/26/1955")]
		public void Record_1ParameterConstructorShouldSucceed(string record)
		{
			PAS.Record r = new PAS.Record(record);

			Assert.NotNull(r);
			Assert.Equal("LastName",                  r.LastName);
			Assert.Equal("FirstName",                 r.FirstName);
			Assert.Equal("Gender",                    r.Gender);
			Assert.Equal("FavoriteColor",             r.FavoriteColor);
			Assert.Equal(DateTime.Parse("2/26/1955"), r.DOB);
		}

		[Theory]
		[InlineData("LastName | FirstName | Gender | FavoriteColor | 2/26/1955", '|')]
		[InlineData("LastName,  FirstName,  Gender,  FavoriteColor,  2/26/1955", ',')]
		[InlineData("LastName FirstName Gender FavoriteColor 2/26/1955", ' ')]
		public void Record_2ParameterConstructorShouldSucceed(string record, char delimiter)
		{
			PAS.Record r = new PAS.Record(record, delimiter);

			Assert.NotNull(r);
			Assert.Equal("LastName",                  r.LastName);
			Assert.Equal("FirstName",                 r.FirstName);
			Assert.Equal("Gender",                    r.Gender);
			Assert.Equal("FavoriteColor",             r.FavoriteColor);
			Assert.Equal(DateTime.Parse("2/26/1955"), r.DOB);
		}

		[Fact]
		public void Record_ToRecordShouldSucceed()
		{
			DataCreator dc = new DataCreator();
			PAS.Record r   = dc.CreateRecord();

			string actual   = r.ToRecord();
			string expected = "LastName        : FirstName       : Gender          : FavoriteColor   : 02/26/1955     ";

			Assert.Equal(expected, actual);
			Assert.Equal(expected.Length, actual.Length);
		}

		[Fact]
		public void Record_CreateHeaderShouldSucceed()
		{
			DataCreator dc = new DataCreator();
			PAS.Record r   = new PAS.Record();

			string actual   = r.CreateHeader();
			string expected = "Last Name       : First Name      : Gender          : Favorite Color  : DOB            ";

			Assert.Equal(expected, actual);
			Assert.Equal(expected.Length, actual.Length);
		}

		[Fact]
		public void Record_ToDelimitedWithDefaultDelimiterShouldSucceed()
		{
			DataCreator dc = new DataCreator();
			PAS.Record r = dc.CreateRecord();

			string actual = r.ToDelimited();
			string expected = "LastName, FirstName, Gender, FavoriteColor, 02/26/1955";

			Assert.Equal(expected, actual);
			Assert.Equal(expected.Length, actual.Length);
		}

		[Fact]
		public void Record_ToDelimitedWithPassedInDelimiterShouldSucceed()
		{
			DataCreator dc = new DataCreator();
			PAS.Record r = dc.CreateRecord();

			string actual = r.ToDelimited(" | ");
			string expected = "LastName | FirstName | Gender | FavoriteColor | 02/26/1955";

			Assert.Equal(expected, actual);
			Assert.Equal(expected.Length, actual.Length);
		}

		[Fact]
		public void Record_CreateUnderlineShouldSucceed()
		{
			DataCreator dc = new DataCreator();
			PAS.Record r = dc.CreateRecord();

			string actual = r.CreateUnderline();
			string expected = "---------       : ----------      : ------          : --------------  : ---            ";

			Assert.Equal(expected, actual);
			Assert.Equal(expected.Length, actual.Length);
		}

		[Fact]
		public void Record_TabShouldWork()
		{
			DataCreator dc = new DataCreator();

			string s = "abcdef";
			object[] args = new object[] { s };

			string actual = (string)pvtObj.Invoke("Tab", args);
			string expected = dc.CreatePadString(s);

			Assert.Equal(expected, actual);
			Assert.Equal(expected.Length, actual.Length);
		}

		[Fact]
		public void Record_CreatePadStringShouldWork()
		{
			object[] args = new object[] { 5 };

			string actual   = (string)pvtObj.Invoke("CreatePadString", args);
			string expected = "     ";

			Assert.Equal(expected, actual);
			Assert.Equal(expected.Length, actual.Length);
		}
	}
}
