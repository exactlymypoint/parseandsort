﻿using Newtonsoft.Json;
using ParseAndSort.Globals.Interfaces;
using ParseAndSort.Globals.POCO;
using System;
using System.Collections.Generic;
using System.Text;
using TestHelper;
using Xunit;
using PAS = ParseAndSort.Globals.POCO;

namespace ParseAndSort.Globals.Test.POCO
{
	public class ParserTest
	{
		private DataCreator dc;
		private string[] testLines;
		private Parser parser;

		public ParserTest()
		{
			dc = new DataCreator();
		}

		[Fact]
		public void Parser_ParseLinesShouldSucceed()
		{
			PAS.Record expectedRecord = dc.CreateRecord();
			testLines = dc.CreateNamesLines();

			parser = new Parser();
			List<PAS.Record> records = (List<PAS.Record>)parser.ParseLines(testLines);

			string expected = JsonConvert.SerializeObject(expectedRecord);

			foreach (PAS.Record r in records)
			{
				string actual = JsonConvert.SerializeObject(r);
				Assert.Equal(expected, actual);
			}
		}
	}
}
