﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;
using Assert = Xunit.Assert;
using SysCon = System.Console;

namespace Console.ParseAndSort.Test
{
	public class ConsoleIOTest
	{
		private PrivateObject pvtObj;
		private ConsoleIO	  cio;
		private string		  CRLF = "\r\n";

		public ConsoleIOTest()
		{
			cio    = new ConsoleIO();
			pvtObj = new PrivateObject(cio);
		}

		[Fact]
		public void ConsoleIO_GetFilenameShouldSucceedWithGoodFileName()
		{
			string filename = "foo.doc";
			string prompt   = "Filename 2: ";
			string getFileName;

			StringWriter output = new StringWriter();
			SysCon.SetOut(output);

			StringReader input = new StringReader(filename);
			SysCon.SetIn(input);

			ConsoleIO consoleIO = new ConsoleIO();

			getFileName = consoleIO.GetFilename(false, 2);

			Assert.Equal(filename, getFileName);
			Assert.Equal(prompt, output.ToString());
		}

		[Fact]
		public void ConsoleIO_GetFilenameShouldSucceedWithBadFileName()
		{
			string filename = "foo.doc";
			string prompt   = CRLF + "File does not exist. Please try again.\r\nFilename 2: "
;
			string getFileName;

			StringWriter output = new StringWriter();
			SysCon.SetOut(output);

			StringReader input = new StringReader(filename);
			SysCon.SetIn(input);

			ConsoleIO consoleIO = new ConsoleIO();

			getFileName = consoleIO.GetFilename(true, 2);

			Assert.Equal(filename, getFileName);
			Assert.Equal(prompt, output.ToString());
		}

		[Fact]
		public void ConsoleIO_PromptShouldSucceed()
		{
			string heading         = "this is a prompt";
			string expectedHeading = CRLF + heading + CRLF;

			StringWriter output = new StringWriter();
			SysCon.SetOut(output);

			object[] args = new object[] { heading };

			pvtObj.Invoke("WritePrompt", args);

			Assert.Equal(expectedHeading, output.ToString());
		}

		[Fact]
		public void ConsoleIO_EndMessageShouldSucceed()
		{
			string expectedOutput = CRLF + "Press ENTER key to end" + CRLF;

			StringWriter output = new StringWriter();
			SysCon.SetOut(output);

			StringReader input = new StringReader(CRLF);
			SysCon.SetIn(input);

			ConsoleIO consoleIO = new ConsoleIO();

			consoleIO.EndMessage();

			Assert.Equal(expectedOutput, output.ToString());
		}
	}
}
